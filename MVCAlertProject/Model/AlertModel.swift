//
//  AlertModel.swift
//  MVCAlertProject
//
//  Created by roman on 7/23/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

struct AlertModel {
    public static let alertModel = AlertModel()
    
    func convertToHello(word: String) -> String {
        if word == "leoHl" {
            return "Hello"
        } else {
            return "LOH"
        }
    }
}
