//
//  ViewController.swift
//  MVCAlertProject
//
//  Created by roman on 7/23/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func startButtonTapped(_ sender: UIButton) {
        self.alertForCheckWord(title: "Check the word", message: "Put the word:", style: .alert)
    }
    
    func alertForCheckWord(title: String, message: String, style: UIAlertController.Style) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let startButtonAlert = UIAlertAction(title: "Check", style: .default) { [weak self] _ in
            self?.label.text = AlertModel.alertModel.convertToHello(word: alertController.textFields?[0].text ?? "")
        }
        alertController.addTextField { (text) in
            text.placeholder = "Put the word"
        }
        alertController.addAction(startButtonAlert)
        self.present(alertController, animated: true, completion: nil)
    }
}

